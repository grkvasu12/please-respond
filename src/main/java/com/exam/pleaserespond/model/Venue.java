package com.exam.pleaserespond.model;

public class Venue {
    private String venue_name;
    private float lon;
    private float lat;
    private float venue_id;


    // Getter Methods

    public String getVenue_name() {
        return venue_name;
    }

    public float getLon() {
        return lon;
    }

    public float getLat() {
        return lat;
    }

    public float getVenue_id() {
        return venue_id;
    }

    // Setter Methods

    public void setVenue_name(String venue_name) {
        this.venue_name = venue_name;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public void setVenue_id(float venue_id) {
        this.venue_id = venue_id;
    }
}