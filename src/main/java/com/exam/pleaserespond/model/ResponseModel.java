package com.exam.pleaserespond.model;

public class ResponseModel {
    Venue VenueObject;
    private String visibility;
    private String response;
    private float guests;
    Member MemberObject;
    private float rsvp_id;
    private float mtime;
    Event EventObject;
    Group GroupObject;


    // Getter Methods

    public Venue getVenue() {
        return VenueObject;
    }

    public String getVisibility() {
        return visibility;
    }

    public String getResponse() {
        return response;
    }

    public float getGuests() {
        return guests;
    }

    public Member getMember() {
        return MemberObject;
    }

    public float getRsvp_id() {
        return rsvp_id;
    }

    public float getMtime() {
        return mtime;
    }

    public Event getEvent() {
        return EventObject;
    }

    public Group getGroup() {
        return GroupObject;
    }

    // Setter Methods

    public void setVenue(Venue venueObject) {
        this.VenueObject = venueObject;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public void setGuests(float guests) {
        this.guests = guests;
    }

    public void setMember(Member memberObject) {
        this.MemberObject = memberObject;
    }

    public void setRsvp_id(float rsvp_id) {
        this.rsvp_id = rsvp_id;
    }

    public void setMtime(float mtime) {
        this.mtime = mtime;
    }

    public void setEvent(Event eventObject) {
        this.EventObject = eventObject;
    }

    public void setGroup(Group groupObject) {
        this.GroupObject = groupObject;
    }
}
