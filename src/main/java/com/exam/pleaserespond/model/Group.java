package com.exam.pleaserespond.model;

import java.util.ArrayList;

public class Group {
    ArrayList < Object > group_topics = new ArrayList< Object >();
    private String group_city;
    private String group_country;
    private float group_id;
    private String group_name;
    private float group_lon;
    private String group_urlname;
    private float group_lat;


    // Getter Methods

    public String getGroup_city() {
        return group_city;
    }

    public String getGroup_country() {
        return group_country;
    }

    public float getGroup_id() {
        return group_id;
    }

    public String getGroup_name() {
        return group_name;
    }

    public float getGroup_lon() {
        return group_lon;
    }

    public String getGroup_urlname() {
        return group_urlname;
    }

    public float getGroup_lat() {
        return group_lat;
    }

    // Setter Methods

    public void setGroup_city(String group_city) {
        this.group_city = group_city;
    }

    public void setGroup_country(String group_country) {
        this.group_country = group_country;
    }

    public void setGroup_id(float group_id) {
        this.group_id = group_id;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public void setGroup_lon(float group_lon) {
        this.group_lon = group_lon;
    }

    public void setGroup_urlname(String group_urlname) {
        this.group_urlname = group_urlname;
    }

    public void setGroup_lat(float group_lat) {
        this.group_lat = group_lat;
    }
}

