package com.exam.pleaserespond.model;

public class Event {
    private String event_name;
    private String event_id;
    private float time;
    private String event_url;


    // Getter Methods

    public String getEvent_name() {
        return event_name;
    }

    public String getEvent_id() {
        return event_id;
    }

    public float getTime() {
        return time;
    }

    public String getEvent_url() {
        return event_url;
    }

    // Setter Methods

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public void setTime(float time) {
        this.time = time;
    }

    public void setEvent_url(String event_url) {
        this.event_url = event_url;
    }
}

