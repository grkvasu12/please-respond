package com.exam.pleaserespond.Service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.*;
import java.net.URL;

@Component
public class ResponseService {

    @Async("asyncExecutor")
    public void makeCall(long endProgram) throws IOException {
        InputStream input = new URL("http://stream.meetup.com/2/rsvps").openStream();
        byte[] buffer = new byte[8 * 1024];
        input.read(buffer);
        int bytesRead;
        File targetFile = new File("src/main/resources/response.txt");
        if (targetFile.exists()) {
            RandomAccessFile raf = new RandomAccessFile(targetFile, "rw");
            raf.setLength(0);
        }
        OutputStream outStream = new FileOutputStream(targetFile);
        while (System.currentTimeMillis() < endProgram && (bytesRead = input.read(buffer)) != -1) {
            outStream.write(buffer, 0, bytesRead);
        }
        outStream.write(buffer);
    }
}
