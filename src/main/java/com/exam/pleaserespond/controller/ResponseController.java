package com.exam.pleaserespond.controller;

import com.exam.pleaserespond.Service.ResponseService;
import com.exam.pleaserespond.model.ResponseModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Scanner;


@RestController
public class ResponseController{

    @Autowired
    ResponseService service;

    @RequestMapping(value = "/stream/{time}", method = RequestMethod.GET, produces = "application/json")
    public StreamingResponseBody streamResponse (@PathVariable("time") int time) throws IOException {
        long t= System.currentTimeMillis();
        long endProgram = t+time*1000;
        service.makeCall(endProgram);
        ObjectMapper mapper = new ObjectMapper();
        return new StreamingResponseBody() {
            @Override
            public void writeTo(OutputStream out) throws IOException {
                int i=0;
                while(System.currentTimeMillis() < endProgram) {
                    InputStream fis = new FileInputStream("src/main/resources/response.txt");
                    Scanner scanner = new Scanner(fis);
                    while (scanner.hasNextLine()) {
                        String line = scanner.nextLine();
                      //  ResponseModel model = mapper.readValue(line, ResponseModel.class);
                        out.write((line).getBytes());
                    }
//                    out.write((Integer.toString(i++) + " - ")
//                            .getBytes());
                    out.flush();
                    try {
                        Thread.sleep(5);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
    }

    @RequestMapping(value = "/stream", method = RequestMethod.GET, produces = "application/json")
    public StreamingResponseBody streamResponse() throws IOException {
        return streamResponse(60);
    }
}